#Kubernetes script for npm-audit
This is the script to deploy npm-audit to Kubernetes.

The OWASP npm-audit container expects environment variables rather than command line arguments to pass values.

```
apiVersion: v1
kind: Pod
metadata:
  name: npm-audit
  labels:
    app: security-pipeline
spec:
  containers:
    - name: npm-audit
      image: rtencatexebia/npm-audit
      env: 
      - name: SOURCE_REPO #Get from vault 
        valueFrom:
          secretKeyRef:
            name: npm-audit-secrets
            key: repository 
      - name: DOJO_API_KEY
        valueFrom:
          secretKeyRef:
            name: npm-audit-secrets
            key: dojo-api-key #Get from vault
      - name: DOJO_URL
        valueFrom:
          secretKeyRef:
            name: npm-audit-secrets
            key: dojo-url #Get as a secret
      - name: DOJO_ENGAGEMENT_ID
        valueFrom:
          secretKeyRef:
            name: npm-audit-secrets
            key: dojo-engagement-id #Get as a secret    
  restartPolicy: Never


```

##Scan configurations 

The config script contains several parameters that are required to run a successful scan:

######SOURCE_REPO

This value refers to the repository that needs to be pulled to be built and scanned. The format for such a token is as follows:

```
https://github.com/xebia/python.git
```

######DOJO_API_KEY

This value refers to the API used to let the container authenticate to Defect dojo when pushing the metrics:

```
<username>:<apikey>
```

######DOJO_URL

The URL of where the Defect-Dojo instance is deployed:

```
https://defectdojo-container.azurewebsites.net
```

######DOJO_ENGAGEMENT_ID

The engagement ID correlating to the engagement in Defect-Dojo you want to commit the metrics to

```
<id>
```